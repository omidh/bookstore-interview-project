import { DateTime, DateTimeUnit } from 'luxon';

/**
 * Convert SBN to ISBN-10
 * @param sbn
 * @returns ISBN-10
 */
export function sbnToIsbn(sbn: string): string {
  return `0${sbn}`;
}

/**
 * Convert date string to an start and range depending on its value.
 * @param dateString
 * @returns `startDate` and `endDate`
 * @example
 * dateRangeFromString('2012'); // returns start and end of the given year
 * dateRangeFromString('2012-01'); // returns start and end of the given month
 * dateRangeFromString('2012-01-04'); // returns start and end of the given day
 */
export function dateRangeFromString(dateString: string) {
  const date = DateTime.fromJSDate(new Date(dateString));
  const [_year, month, day] = dateString.split('-');
  let unit: DateTimeUnit;
  if (day) {
    unit = 'day';
  } else if (month) {
    unit = 'month';
  } else {
    unit = 'year';
  }

  return {
    startDate: date.startOf(unit).toJSDate(),
    endDate: date.endOf(unit).toJSDate(),
  };
}
