/**
 * Removes extra spaces between words.
 *
 * Note that it does not trim the string.
 *
 * @param value
 * @returns original string without extra spaces.
 */
export function removeExtraSpaces(value: string): string {
  return value.replace(/\s+/g, ' ');
}
