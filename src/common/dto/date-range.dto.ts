import { IsDate, IsNotEmpty } from 'class-validator';

export class DateRangeDto {
  @IsNotEmpty()
  @IsDate()
  readonly startDate!: Date;

  @IsNotEmpty()
  @IsDate()
  readonly endDate!: Date;
}
