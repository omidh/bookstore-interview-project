import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNotEmpty, IsInt, IsPositive, Min } from 'class-validator';

export class PaginationDto {
  @ApiProperty({ type: 'integer', minimum: 1, nullable: false })
  @IsNotEmpty()
  @IsInt()
  @IsPositive()
  @Min(1)
  @Type(() => Number)
  readonly limit!: number;

  @ApiProperty({
    type: 'integer',
    minimum: 0,
    nullable: false,
    description: 'Pages start at 0',
  })
  @IsNotEmpty()
  @IsInt()
  @Min(0)
  @Type(() => Number)
  readonly page!: number;
}
