import {
  Transform,
  TransformFnParams,
  TransformOptions,
} from 'class-transformer';

/**
 * Transform the given string to lowercase.
 */
export function ToLowerCase(
  transformOptions?: TransformOptions,
): (target: any, key: string) => void {
  return Transform((params: TransformFnParams) => {
    if ('string' !== typeof params.value) {
      return params.value;
    }

    return params.value.toLowerCase();
  }, transformOptions);
}
