import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { AppSetup } from './app.setup';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const appSetup = new AppSetup(app);
  appSetup.setup();
  const configService=  app.get<ConfigService>(ConfigService);
  const port = configService.getOrThrow('PORT')
  await app.listen(port);
}
bootstrap();
