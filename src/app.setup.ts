import { INestApplication, ValidationPipe } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { Book } from './book/schemas/book.schema';

export class AppSetup {
  constructor(private readonly app: INestApplication) {}

  setup(): void {
    this.setupClassValidator();
    this.setupSwagger();
  }

  private setupClassValidator(): void {
    this.app.useGlobalPipes(
      new ValidationPipe({
        whitelist: true,
        transform: true,
        forbidUnknownValues: true,
        forbidNonWhitelisted: true,
      }),
    );
  }

  private setupSwagger(): void {
    const options = new DocumentBuilder()
      .setTitle('BookStore Interview')
      .setDescription('BookStore Interview API.')
      .setVersion('1.0')
      .build();
    const document = SwaggerModule.createDocument(this.app, options, {
      extraModels: [Book],
    });
    SwaggerModule.setup('api', this.app, document);
  }
}
