import * as Joi from 'joi';

export const appConfig = {
  cache: true,
  validationSchema: Joi.object({
    NODE_ENV: Joi.string()
      .required()
      .valid('development', 'production', 'test', 'provision'),
    MONGO_URI: Joi.string().required().uri(),
    PORT: Joi.number().port().required(),
  }),
};
