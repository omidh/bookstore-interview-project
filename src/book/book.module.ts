import { Module } from '@nestjs/common';
import { BookConsumer } from './book-consumer';
import { BookRepository } from './book-repository';
import { BookService } from './book.service';
import { BookController } from './book.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Book, BookSchema } from './schemas/book.schema';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([{ name: Book.name, schema: BookSchema }]),
  ],
  providers: [BookConsumer, BookRepository, BookService],
  controllers: [BookController],
})
export class BookModule {}
