import { HttpService } from '@nestjs/axios';
import { UnprocessableEntityException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { of } from 'rxjs';
import { rawBookGenerator } from '../../test/mock-generators/raw-book.generator';
import { BookConsumer } from './book-consumer';
import { BookRepository } from './book-repository';
import { RawBook } from './models/raw-book.model';
import { Book } from './schemas/book.schema';

describe('BookConsumer', () => {
  let provider: BookConsumer;
  let httpService: HttpService;
  let bookRepository: BookRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BookConsumer,
        {
          provide: HttpService,
          useValue: {
            get: jest.fn(),
          },
        },
        {
          provide: BookRepository,
          useValue: {
            clear: jest.fn().mockResolvedValue(undefined),
            insertMany: jest.fn().mockResolvedValue(undefined),
          },
        },
      ],
    }).compile();

    provider = module.get<BookConsumer>(BookConsumer);
    httpService = module.get<HttpService>(HttpService);
    bookRepository = module.get<BookRepository>(BookRepository);
  });

  it('should be defined', () => {
    expect(provider).toBeDefined();
  });

  describe('import', () => {
    let rawBook: RawBook;
    beforeEach(() => {
      rawBook = rawBookGenerator();
      jest
        .spyOn(httpService, 'get')
        .mockReturnValue(of({ data: [rawBook] } as any));
    });

    it('should call BookRepository clear method', async () => {
      await provider.import({ url: 'www.example.com' });
      expect(bookRepository.clear).toHaveBeenCalled();
    });

    it("should use HttpSevice's get method to download sample data from the given URL as JSON data", async () => {
      await provider.import({ url: 'www.example.com' });
      expect(httpService.get).toHaveBeenCalledWith('www.example.com', {
        headers: { 'Content-Type': 'application/json' },
      });
    });

    it('should insert fetched data into database with BookRepository insertMany method', async () => {
      await provider.import({ url: 'www.example.com' });
      const expectedBook: Book = {
        authors: rawBook.authors,
        categories: rawBook.categories,
        currency: rawBook.published.currency,
        pageCount: rawBook.pageCount,
        price: rawBook.published.price,
        publishedAt: rawBook.published.$date,
        title: rawBook.title,
        isbn: rawBook.isbn,
        longDescription: rawBook.longDescription,
        shortDescription: rawBook.shortDescription,
        thumbnailUrl: rawBook.thumbnailUrl,
      };
      expect(bookRepository.insertMany).toHaveBeenCalledWith([expectedBook]);
    });

    it('should throw UnprocessableEntityException if fetched data sample has missing required field', async () => {
      jest
        .spyOn(httpService, 'get')
        .mockReturnValue(
          of({ data: [{ title: 'harry potter', isbn: '1234567890' }] } as any),
        );
      await expect(provider.import({ url: 'www.example.com' })).rejects.toThrow(
        UnprocessableEntityException,
      );
    });

    it('should throw UnprocessableEntityException if fetched data sample has missing required field', async () => {
      jest
        .spyOn(httpService, 'get')
        .mockReturnValue(
          of({ data: [{ title: 'harry potter', isbn: '1234567890' }] } as any),
        );
      await expect(provider.import({ url: 'www.example.com' })).rejects.toThrow(
        UnprocessableEntityException,
      );
    });
  });
});
