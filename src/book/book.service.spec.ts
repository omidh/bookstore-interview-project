import { Test, TestingModule } from '@nestjs/testing';
import { bookGenerator } from '../../test/mock-generators/book.generator';
import { BookRepository } from './book-repository';
import { BookService } from './book.service';

describe('BookService', () => {
  let service: BookService;
  let bookRepository: BookRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BookService,
        {
          provide: BookRepository,
          useValue: {
            findMany: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<BookService>(BookService);
    bookRepository = module.get<BookRepository>(BookRepository);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findMany', () => {
    let foundBooks: Awaited<ReturnType<BookRepository['findMany']>>;
    beforeEach(() => {
      foundBooks = {
        items: [bookGenerator()],
        limit: 10,
        page: 1,
        total: 50,
      };
      jest.spyOn(bookRepository, 'findMany').mockResolvedValue(foundBooks);
    });

    it("should call BookRepository's findMany method and return it", async () => {
      const findParams: Required<Parameters<BookRepository['findMany']>>[0] = {
        limit: 10,
        page: 1,
        author: 'someAuthor',
        category: 'programming',
        currency: 'USD',
        description: 'someDesc',
        isbn: '1234567890',
        pageCount: 100,
        price: 40,
        publishedAt: { gte: new Date(2010), lte: new Date(2011) },
        thumbnailUrl: 'https://www.example.com/image.jpg',
        title: 'someTitle',
      };

      await expect(service.findMany(findParams)).resolves.toEqual(foundBooks);
      expect(bookRepository.findMany).toHaveBeenCalledWith(findParams);
    });
  });
});
