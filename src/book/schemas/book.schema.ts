import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { compact } from 'lodash';
import { removeExtraSpaces } from '../../common/helpers/transformers';
import { sbnToIsbn } from '../../common/helpers/utilities';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export type BookDocument = Book & Document;

@Schema()
export class Book {
  @ApiProperty({
    type: 'string',
    maxLength: 100,
    minLength: 1,
    nullable: false,
  })
  @Prop({
    type: String,
    required: true,
    trim: true,
    index: true,
    lowercase: true,
    minlength: 1,
    maxlength: 200,
  })
  readonly title!: string;

  @ApiPropertyOptional({
    type: 'string',
    format: 'url',
    nullable: true,
    example: 'https://www.example.com/image.jpg',
  })
  @Prop({
    type: String,
    trim: true,
    lowercase: true,
  })
  readonly thumbnailUrl?: string | null;

  @ApiPropertyOptional({ type: 'string', nullable: true })
  @Prop({
    type: String,
    trim: true,
    set: (value: string) => (value ? removeExtraSpaces(value) : value),
  })
  readonly longDescription?: string | null;

  @ApiPropertyOptional({ type: 'string', nullable: true })
  @Prop({
    type: String,
    trim: true,
    set: (value: string) => (value ? removeExtraSpaces(value) : value),
  })
  readonly shortDescription?: string | null;

  @ApiPropertyOptional({
    type: 'string',
    nullable: true,
    example: '1933988673',
  })
  @Prop({
    type: String,
    index: true,
    minlength: 10,
    maxlength: 13,
    set: (value: string) => (value?.length === 9 ? sbnToIsbn(value) : value),
  })
  readonly isbn?: string | null;

  @ApiProperty({ type: 'integer', minimum: 0, nullable: false })
  @Prop({
    type: Number,
    required: true,
    index: true,
    min: 0,
    validate: {
      validator: Number.isInteger,
      message: 'pageCount must be an integer but received {VALUE}.',
    },
  })
  readonly pageCount!: number;

  @ApiProperty({
    type: 'string',
    isArray: true,
    maxLength: 100,
    nullable: false,
  })
  @Prop({
    type: [{ type: String, trim: true, maxlength: 100, lowercase: true }],
    index: true,
    required: true,
    set: (value: string[]) => compact(value),
  })
  readonly authors!: string[];

  @ApiProperty({
    type: 'string',
    isArray: true,
    maxLength: 100,
    nullable: false,
  })
  @Prop({
    type: [{ type: String, trim: true, maxlength: 100, lowercase: true }],
    index: true,
    required: true,
    set: (value: string[]) => compact(value),
  })
  readonly categories!: string[];

  @ApiProperty({ type: 'number', minimum: 0, nullable: false })
  @Prop({
    type: Number,
    required: true,
    min: 0,
    index: true,
  })
  readonly price!: number;

  @ApiProperty({
    type: 'string',
    minLength: 3,
    maxLength: 3,
    nullable: false,
    example: 'USD',
  })
  @Prop({
    type: String,
    minlength: 3,
    maxLength: 3,
    match: /^[a-zA-Z]*$/,
    index: true,
    required: true,
    uppercase: true,
  })
  readonly currency!: string;

  @ApiProperty({ type: 'string', format: 'date-time', nullable: false })
  @Prop({
    type: Date,
    index: true,
    required: true,
  })
  readonly publishedAt!: Date;
}

export const BookSchema = SchemaFactory.createForClass(Book);

BookSchema.index({
  longDescription: 'text',
  shortDescription: 'text',
});
