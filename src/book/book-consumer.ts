import { HttpService } from '@nestjs/axios';
import {
  Injectable,
  Logger,
  UnprocessableEntityException,
} from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { validateOrReject } from 'class-validator';
import { firstValueFrom } from 'rxjs';
import { BookRepository } from './book-repository';
import { RawBook } from './models/raw-book.model';
import { Book } from './schemas/book.schema';

@Injectable()
export class BookConsumer {
  private readonly logger = new Logger(BookConsumer.name);

  constructor(
    private readonly httpService: HttpService,
    private readonly bookRepository: BookRepository,
  ) {}

  /**
   * Populate Book collection from an external source.
   *
   * Note that it clears previous book documents.
   * @param params
   */
  async import(params: { url: string }): Promise<void> {
    await this.bookRepository.clear();
    let rawBooks: RawBook[];
    try {
      const data = await this.downloadFromUrl(params.url);
      rawBooks = await Promise.all(
        data.map((bookData) => this.parse(bookData)),
      );
    } catch (error) {
      this.logger.warn(`Failed to retrieve data samples from ${params.url}`);
      this.logger.warn(error);
      throw new UnprocessableEntityException(error);
    }

    const books = rawBooks.map((rawBook) => this.toBookSchema(rawBook));
    await this.bookRepository.insertMany(books);
  }

  /**
   * Return `Book` from the given `RawBook`
   * @param rawBook
   * @returns `Book`
   */
  private toBookSchema(rawBook: RawBook): Book {
    return {
      authors: rawBook.authors,
      categories: rawBook.categories,
      currency: rawBook.published.currency,
      isbn: rawBook.isbn,
      pageCount: rawBook.pageCount,
      price: rawBook.published.price,
      publishedAt: rawBook.published.$date,
      thumbnailUrl: rawBook.thumbnailUrl,
      title: rawBook.title,
      longDescription: rawBook.longDescription,
      shortDescription: rawBook.shortDescription,
    };
  }

  /**
   * Download book data from an extrnal URL to the memory.
   *
   * Note that this method should not be used for large data samples.
   * @param url
   * @returns fetched data
   */
  private async downloadFromUrl(url: string): Promise<Array<unknown>> {
    const data = (
      await firstValueFrom(
        this.httpService.get(url, {
          headers: { 'Content-Type': 'application/json' },
        }),
      )
    ).data;
    if (!Array.isArray(data)) {
      throw new UnprocessableEntityException('Books data must be an array.');
    }

    return data;
  }

  /**
   * Validate and transform fetched data item to `RawBook` model.
   * @param bookData
   * @returns `rawBook` if passed data is valid
   */
  private async parse(bookData: unknown): Promise<RawBook> {
    const rawBook = plainToClass(RawBook, bookData);
    await validateOrReject(rawBook);
    return rawBook;
  }
}
