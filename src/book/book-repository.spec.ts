import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { Model } from 'mongoose';
import { bookGenerator } from '../../test/mock-generators/book.generator';
import { BookRepository } from './book-repository';
import { Book, BookDocument } from './schemas/book.schema';

describe('BookRepository', () => {
  let provider: BookRepository;
  let bookModel: Model<BookDocument>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BookRepository,
        {
          provide: getModelToken(Book.name),
          useValue: {
            insertMany: jest.fn(),
            deleteMany: jest.fn(),
            countDocuments: jest.fn(),
            find: jest.fn(),
          },
        },
      ],
    }).compile();

    provider = module.get<BookRepository>(BookRepository);
    bookModel = module.get<Model<BookDocument>>(getModelToken(Book.name));
  });

  it('should be defined', () => {
    expect(provider).toBeDefined();
  });

  describe('insertMany', () => {
    const book = bookGenerator();
    beforeEach(() => {
      jest.spyOn(bookModel, 'insertMany').mockResolvedValue([book]);
    });

    it("should call Book model's insertMany method with given book object", async () => {
      await provider.insertMany([book]);
      expect(bookModel.insertMany).toHaveBeenCalledWith([book]);
    });
  });

  describe('clear', () => {
    beforeEach(() => {
      jest
        .spyOn(bookModel, 'deleteMany')
        .mockResolvedValue({ acknowledged: true, deletedCount: 10 });
    });

    it("should call Book model's deleteMany method without any argument", async () => {
      await provider.clear();
      expect(bookModel.deleteMany).toHaveBeenCalledWith();
    });
  });

  describe('findMany', () => {
    let queryBuilder: any;
    let foundBooks: Awaited<ReturnType<BookRepository['findMany']>>;
    beforeEach(() => {
      foundBooks = {
        items: [bookGenerator()],
        limit: 10,
        page: 1,
        total: 50,
      };
      queryBuilder = {
        where: jest.fn().mockReturnThis(),
        equals: jest.fn().mockReturnThis(),
        getQuery: jest.fn().mockReturnValue('mockQuery'),
      };

      jest
        .spyOn(bookModel, 'find')
        .mockReturnValueOnce(queryBuilder)
        .mockReturnValue({
          sort: jest.fn().mockReturnThis(),
          skip: jest.fn().mockReturnThis(),
          limit: jest.fn().mockResolvedValue(foundBooks.items),
        } as any);
      jest.spyOn(bookModel, 'countDocuments').mockResolvedValue(1);
    });

    it('should count documents and return it', async () => {
      await expect(provider.findMany({ limit: 10, page: 1 })).resolves.toEqual(
        expect.objectContaining({ total: 1 }),
      );
      expect(bookModel.countDocuments).toHaveBeenCalledWith('mockQuery');
    });

    it('should return given pagination options', async () => {
      await expect(provider.findMany({ limit: 10, page: 1 })).resolves.toEqual(
        expect.objectContaining({ page: 1, limit: 10 }),
      );
    });

    it('should return book documents', async () => {
      await expect(provider.findMany({ limit: 10, page: 1 })).resolves.toEqual(
        expect.objectContaining({ items: foundBooks.items }),
      );
    });

    it('should do a full text search if description criteria is given', async () => {
      await provider.findMany({ limit: 10, page: 1, description: 'ASP.net' });
      expect(queryBuilder.where).toHaveBeenCalledWith('$text', {
        $search: 'ASP.net',
      });
    });

    it('should check agents field for equality if agent criteria is given', async () => {
      await provider.findMany({ limit: 10, page: 1, author: 'Jk Rowling' });
      expect(queryBuilder.where).toHaveBeenCalledWith('authors');
      expect(queryBuilder.equals).toHaveBeenCalledWith('Jk Rowling');
    });

    it('should check categories field for equality if category criteria is given', async () => {
      await provider.findMany({ limit: 10, page: 1, category: 'programming' });
      expect(queryBuilder.where).toHaveBeenCalledWith('categories');
      expect(queryBuilder.equals).toHaveBeenCalledWith('programming');
    });

    it('should check price field for equality if price criteria is given', async () => {
      await provider.findMany({ limit: 10, page: 1, price: 100 });
      expect(queryBuilder.where).toHaveBeenCalledWith('price');
      expect(queryBuilder.equals).toHaveBeenCalledWith(100);
    });

    it('should check pageCount field for equality if pageCount criteria is given', async () => {
      await provider.findMany({ limit: 10, page: 1, pageCount: 100 });
      expect(queryBuilder.where).toHaveBeenCalledWith('pageCount');
      expect(queryBuilder.equals).toHaveBeenCalledWith(100);
    });

    it('should check thumbnailUrl field for equality if thumbnailUrl criteria is given', async () => {
      await provider.findMany({
        limit: 10,
        page: 1,
        thumbnailUrl: 'https://www.example.com/image.jpg',
      });
      expect(queryBuilder.where).toHaveBeenCalledWith('thumbnailUrl');
      expect(queryBuilder.equals).toHaveBeenCalledWith(
        'https://www.example.com/image.jpg',
      );
    });

    it('should check currency field for equality if currency criteria is given', async () => {
      await provider.findMany({ limit: 10, page: 1, currency: 'USD' });
      expect(queryBuilder.where).toHaveBeenCalledWith('currency');
      expect(queryBuilder.equals).toHaveBeenCalledWith('USD');
    });

    it('should check isbn field for equality if isbn criteria is given', async () => {
      await provider.findMany({ limit: 10, page: 1, isbn: '1234567890' });
      expect(queryBuilder.where).toHaveBeenCalledWith('isbn');
      expect(queryBuilder.equals).toHaveBeenCalledWith('1234567890');
    });

    it('should check title field for startsWith equality if title criteria is given', async () => {
      await provider.findMany({ limit: 10, page: 1, title: 'harryPotter' });
      expect(queryBuilder.where).toHaveBeenCalledWith('title');
      expect(queryBuilder.equals).toHaveBeenCalledWith(
        new RegExp('^harryPotter'),
      );
    });

    it('should check publishedAt field for being in the date range if publishedAt criteria is given', async () => {
      await provider.findMany({
        limit: 10,
        page: 1,
        publishedAt: { gte: new Date(2010), lte: new Date(2022) },
      });
      expect(queryBuilder.where).toHaveBeenCalledWith('publishedAt');
      expect(queryBuilder.equals).toHaveBeenCalledWith({
        $gte: new Date(2010),
        $lte: new Date(2022),
      });
    });
  });
});
