import { Type } from 'class-transformer';
import {
  IsArray,
  IsDate,
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  IsUrl,
  ValidateNested,
} from 'class-validator';

class published {
  @IsNotEmpty()
  @IsNumber()
  readonly price!: number;

  @IsNotEmpty()
  @IsString()
  readonly currency!: string;

  @IsNotEmpty()
  @IsDate()
  @Type(() => Date)
  readonly $date!: Date;
}

export class RawBook {
  @IsNotEmpty()
  @IsString()
  readonly title!: string;

  @IsNotEmpty()
  @IsString()
  readonly status!: string;

  @IsOptional()
  @IsString()
  readonly shortDescription?: string | null;

  @IsOptional()
  @IsString()
  readonly longDescription?: string | null;

  @IsOptional()
  @IsUrl()
  readonly thumbnailUrl?: string | null;

  @IsNotEmpty()
  @ValidateNested()
  readonly published!: published;

  @IsOptional()
  @IsString()
  readonly isbn?: string | null;

  @IsNotEmpty()
  @IsInt()
  readonly pageCount!: number;

  @IsNotEmpty()
  @IsArray()
  @IsString({ each: true })
  readonly authors!: string[];

  @IsNotEmpty()
  @IsArray()
  @IsString({ each: true })
  readonly categories!: string[];
}
