import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Put,
  Query,
} from '@nestjs/common';
import {
  ApiNoContentResponse,
  ApiOkResponse,
  ApiTags,
  getSchemaPath,
} from '@nestjs/swagger';
import { PaginationResult } from '../common/types/pagination-result.type';
import { BookConsumer } from './book-consumer';
import { BookService } from './book.service';
import { BookImportDto } from './dto/book-import.dto';
import { BookSearchDto } from './dto/book-search.dto';
import { Book, BookDocument } from './schemas/book.schema';

@ApiTags('Book')
@Controller('book')
export class BookController {
  constructor(
    private readonly bookConsumer: BookConsumer,
    private readonly bookService: BookService,
  ) {}

  @ApiNoContentResponse({ description: 'Books have been imported.' })
  @Put('import')
  @HttpCode(HttpStatus.NO_CONTENT)
  async import(@Body() body: BookImportDto): Promise<void> {
    return this.bookConsumer.import({ url: body.url });
  }

  @ApiOkResponse({
    description: 'Books have been fetched.',
    schema: {
      type: 'object',
      properties: {
        items: {
          type: 'array',
          uniqueItems: true,
          items: {
            $ref: getSchemaPath(Book),
          },
        },
        total: { type: 'integer', minimum: 0 },
        page: { type: 'integer', minimum: 0 },
        limit: { type: 'integer', minimum: 1 },
      },
    },
  })
  @Get('search')
  async search(
    @Query() query: BookSearchDto,
  ): Promise<PaginationResult<BookDocument>> {
    const publishedAt = query.publishedAt
      ? { gte: query.publishedAt.startDate, lte: query.publishedAt.endDate }
      : undefined;
    return this.bookService.findMany({ ...query, publishedAt });
  }
}
