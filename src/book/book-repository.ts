import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Query } from 'mongoose';
import { PaginationResult } from '../common/types/pagination-result.type';
import { BookFindManyArgs } from './interfaces/book-find-many-args.interface';
import { Book, BookDocument } from './schemas/book.schema';

@Injectable()
export class BookRepository {
  constructor(
    @InjectModel(Book.name)
    private readonly bookModel: Model<BookDocument>,
  ) {}

  /**
   * Create one or more Book documents.
   * @param books
   */
  async insertMany(books: Book[]): Promise<void> {
    await this.bookModel.insertMany(books);
  }

  /**
   * Remove all book documents.
   */
  async clear(): Promise<void> {
    await this.bookModel.deleteMany();
  }

  /**
   * Find zero or more Books that matches the filter.
   * @param params
   * @returns found book douments
   */
  async findMany(
    params: BookFindManyArgs,
  ): Promise<PaginationResult<BookDocument>> {
    const queryBuilder = this.bookModel.find();
    this.applyFilterIfDefined(params, queryBuilder);
    const query = queryBuilder.getQuery();
    const items = await this.bookModel
      .find({ ...query })
      .sort({ publishedAt: -1 })
      .skip(params.page * params.limit)
      .limit(params.limit);
    const total = await this.bookModel.countDocuments(query);
    return {
      items,
      total,
      limit: params.limit,
      page: params.page,
    };
  }

  private applyFilterIfDefined(
    params: BookFindManyArgs,
    queryBuilder: Query<
      BookDocument[],
      BookDocument,
      Record<any, any>,
      BookDocument
    >,
  ) {
    params.author && queryBuilder.where('authors').equals(params.author);

    params.category && queryBuilder.where('categories').equals(params.category);

    Number.isFinite(params.price) &&
      queryBuilder.where('price').equals(params.price);

    Number.isSafeInteger(params.pageCount) &&
      queryBuilder.where('pageCount').equals(params.pageCount);

    params.thumbnailUrl &&
      queryBuilder.where('thumbnailUrl').equals(params.thumbnailUrl);

    params.isbn && queryBuilder.where('isbn').equals(params.isbn);

    params.description &&
      queryBuilder.where('$text', { $search: params.description });

    params.publishedAt &&
      queryBuilder.where('publishedAt').equals({
        $gte: params.publishedAt.gte,
        $lte: params.publishedAt.lte,
      });

    params.title &&
      queryBuilder.where('title').equals(new RegExp(`^${params.title}`));

    params.currency && queryBuilder.where('currency').equals(params.currency);
  }
}
