import { Injectable } from '@nestjs/common';
import { PaginationResult } from '../common/types/pagination-result.type';
import { BookRepository } from './book-repository';
import { BookFindManyArgs } from './interfaces/book-find-many-args.interface';
import { BookDocument } from './schemas/book.schema';

@Injectable()
export class BookService {
  constructor(private readonly bookRepository: BookRepository) {}

  /**
   * Find zero or more Books that matches the filter.
   * @param params
   * @returns array of book douments
   */
  async findMany(
    params: BookFindManyArgs,
  ): Promise<PaginationResult<BookDocument>> {
    return this.bookRepository.findMany(params);
  }
}
