export interface BookFindManyArgs {
  readonly title?: string;
  readonly description?: string;
  readonly author?: string;
  readonly category?: string;
  readonly thumbnailUrl?: string;
  readonly currency?: string;
  readonly price?: number;
  readonly isbn?: string;
  readonly pageCount?: number;
  readonly publishedAt?: { lte: Date; gte: Date };
  readonly limit: number;
  readonly page: number;
}
