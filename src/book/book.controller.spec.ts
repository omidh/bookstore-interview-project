import { Test, TestingModule } from '@nestjs/testing';
import { bookGenerator } from '../../test/mock-generators/book.generator';
import { BookConsumer } from './book-consumer';
import { BookController } from './book.controller';
import { BookService } from './book.service';

describe('BookController', () => {
  let controller: BookController;
  let bookConsumer: BookConsumer;
  let bookService: BookService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BookController],
      providers: [
        {
          provide: BookConsumer,
          useValue: {
            import: jest.fn(),
          },
        },
        {
          provide: BookService,
          useValue: {
            findMany: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<BookController>(BookController);
    bookConsumer = module.get<BookConsumer>(BookConsumer);
    bookService = module.get<BookService>(BookService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('import', () => {
    it('should call BookServcie import method with the given URL', async () => {
      await controller.import({ url: 'www.example.com/books.json' });
      expect(bookConsumer.import).toHaveBeenCalledWith({
        url: 'www.example.com/books.json',
      });
    });
  });

  describe('search', () => {
    let foundBooks: Awaited<ReturnType<BookService['findMany']>>;
    beforeEach(() => {
      foundBooks = {
        items: [bookGenerator()],
        limit: 10,
        page: 1,
        total: 50,
      };
      jest.spyOn(bookService, 'findMany').mockResolvedValue(foundBooks);
    });

    it("should call BookRepository's findMany method and return it", async () => {
      const searchQuery: Required<Parameters<BookController['search']>>[0] = {
        limit: 10,
        page: 1,
        author: 'someAuthor',
        category: 'programming',
        currency: 'USD',
        description: 'someDesc',
        isbn: '1234567890',
        pageCount: 100,
        price: 40,
        publishedAt: { startDate: new Date(2010), endDate: new Date(2011) },
        thumbnailUrl: 'https://www.example.com/image.jpg',
        title: 'someTitle',
      };

      await expect(controller.search(searchQuery)).resolves.toEqual(foundBooks);
      expect(bookService.findMany).toHaveBeenCalledWith({
        ...searchQuery,
        publishedAt: {
          gte: searchQuery.publishedAt?.startDate,
          lte: searchQuery.publishedAt?.endDate,
        },
      });
    });
  });
});
