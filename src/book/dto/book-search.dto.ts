import { ApiPropertyOptional } from '@nestjs/swagger';
import { plainToClass, Transform, Type } from 'class-transformer';
import {
  IsCurrency,
  IsInt,
  IsNotEmptyObject,
  IsOptional,
  IsUrl,
  MaxLength,
  Min,
  MinLength,
  ValidateNested,
} from 'class-validator';
import { ToLowerCase } from '../../common/decorators/to-lower-case.decorator';
import { DateRangeDto } from '../../common/dto/date-range.dto';
import { PaginationDto } from '../../common/dto/pagination.dto';
import { dateRangeFromString } from '../../common/helpers/utilities';

export class BookSearchDto extends PaginationDto {
  @ApiPropertyOptional({ maxLength: 100, nullable: false })
  @IsOptional()
  @MaxLength(100)
  readonly description?: string;

  @ApiPropertyOptional({ maxLength: 200, nullable: false })
  @IsOptional()
  @MaxLength(200)
  @ToLowerCase()
  readonly title?: string;

  @ApiPropertyOptional({ nullable: false })
  @IsOptional()
  @IsCurrency()
  readonly currency?: string;

  @ApiPropertyOptional({ nullable: false, format: 'url' })
  @IsOptional()
  @IsUrl()
  @ToLowerCase()
  readonly thumbnailUrl?: string;

  @ApiPropertyOptional({ type: 'integer', minimum: 0 })
  @IsOptional()
  @IsInt()
  @Min(0)
  @Type(() => Number)
  readonly pageCount?: number;

  @ApiPropertyOptional({ type: 'number', minimum: 0 })
  @IsOptional()
  @Min(0)
  @Type(() => Number)
  readonly price?: number;

  @ApiPropertyOptional({ maxLength: 100, nullable: false })
  @IsOptional()
  @MaxLength(100)
  @ToLowerCase()
  readonly author?: string;

  @ApiPropertyOptional({ maxLength: 100, nullable: false })
  @IsOptional()
  @MaxLength(100)
  @ToLowerCase()
  readonly category?: string;

  @ApiPropertyOptional({ maxLength: 13, minLength: 10, nullable: false })
  @IsOptional()
  @MaxLength(13)
  @MinLength(10)
  readonly isbn?: string;

  @ApiPropertyOptional({
    type: 'string',
    format: 'date',
    nullable: true,
    example: '2020-01-23',
  })
  @IsOptional()
  @IsNotEmptyObject()
  @ValidateNested()
  @Transform(({ value }) => {
    if (!value || typeof value !== 'string') {
      return;
    }

    const { startDate, endDate } = dateRangeFromString(value);
    return plainToClass(DateRangeDto, {
      startDate,
      endDate,
    });
  })
  readonly publishedAt?: DateRangeDto | null;
}
