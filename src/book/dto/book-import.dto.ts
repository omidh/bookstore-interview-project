import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUrl } from 'class-validator';

export class BookImportDto {
  @ApiProperty({ type: 'string', format: 'url', nullable: false })
  @IsNotEmpty()
  @IsUrl()
  readonly url!: string;
}
