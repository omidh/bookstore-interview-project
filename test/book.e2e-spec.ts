import { Test, TestingModule } from '@nestjs/testing';
import { NestExpressApplication } from '@nestjs/platform-express';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { AppSetup } from '../src/app.setup';
import { BookConsumer } from '../src/book/book-consumer';
import { BookSearchDto } from '../src/book/dto/book-search.dto';
import { HttpStatus } from '@nestjs/common';

describe('BookController (e2e)', () => {
  let app: NestExpressApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication<NestExpressApplication>();
    const appSetup = new AppSetup(app);
    appSetup.setup();
    await app.init();
    const sampleDataUrl =
      'https://run.mocky.io/v3/d7f02fdc-5591-4080-a163-95a08ce6895e';
    const bookConsumer = app.get<BookConsumer>(BookConsumer);
    await bookConsumer.import({ url: sampleDataUrl });
  });

  afterAll(async () => {
    await app.close();
  });

  it('/search?category=Java (GET)', async () => {
    const query: BookSearchDto = {
      limit: 100,
      page: 0,
      category: 'Java',
    };
    const response = await request(app.getHttpServer())
      .get('/book/search')
      .query(query)
      .expect(HttpStatus.OK);
    // since this query returns large data, I skipped checking their ISBN,
    // However I did it for other tests.
    expect(response.body?.items).toHaveLength(30);
  });

  it('/search?publishedAt=2011 (GET)', async () => {
    const query: BookSearchDto = {
      limit: 100,
      page: 0,

      /**
       * we can pass simple data string for publishedAt
       * even though it expects `DateRangeDto`.
       * That is because it automatically transforms our date string
       * to expected `DateRangeDto`.
       *
       * So no harm using any in this case.
       */
      publishedAt: '2011' as any,
    };
    const response = await request(app.getHttpServer())
      .get('/book/search')
      .query(query)
      .expect(HttpStatus.OK);
    expect(response.body?.items).toHaveLength(6);
    expect(response.body.items).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ isbn: '1935182870' }),
        expect.objectContaining({ isbn: '193518217X' }),
        expect.objectContaining({ isbn: '1935182927' }),
        expect.objectContaining({ isbn: '1617290084' }),
        expect.objectContaining({ isbn: '1935182463' }),
        expect.objectContaining({ isbn: '1935182722' }),
      ]),
    );
  });

  it('/search?publishedAt=2009-04-01&price=40 (GET)', async () => {
    const query: BookSearchDto = {
      limit: 100,
      page: 0,
      price: 40,
      publishedAt: '2009-04-01' as any,
    };
    const response = await request(app.getHttpServer())
      .get('/book/search')
      .query(query)
      .expect(HttpStatus.OK);
    expect(response.body?.items).toHaveLength(2);
    expect(response.body.items).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ isbn: '1933988673' }),
        expect.objectContaining({ isbn: '1933988657' }),
      ]),
    );
  });
});
