import { random } from 'lodash';
import mongoose from 'mongoose';
import { generate } from 'randomstring';
import { Book, BookDocument } from '../../src/book/schemas/book.schema';

export function bookGenerator(
  options: Partial<Book & { _id: mongoose.Types.ObjectId }> = {},
): BookDocument {
  return {
    _id: options._id ?? new mongoose.Types.ObjectId(),
    title: options.title ?? generate({ length: 20 }),
    currency: options.currency ?? 'USD',
    longDescription: options.longDescription ?? undefined,
    shortDescription: options.shortDescription ?? undefined,
    thumbnailUrl: options.thumbnailUrl ?? 'https://somewebiste.com/image.jpg',
    isbn: options.isbn ?? generate({ length: 10, charset: 'numeric' }),
    pageCount: options.pageCount ?? random(100, 500),
    categories: options.categories ?? [
      generate({ length: 20, charset: 'alphabetic' }),
    ],
    authors: options.authors ?? [
      generate({ length: 20, charset: 'alphabetic' }),
    ],
    publishedAt: options.publishedAt ?? new Date(),
  } as BookDocument;
}
