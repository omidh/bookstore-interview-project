import { random } from 'lodash';
import { generate } from 'randomstring';
import { RawBook } from '../../src/book/models/raw-book.model';

export function rawBookGenerator(options: Partial<RawBook> = {}): RawBook {
  return {
    title: options.title ?? generate({ length: 20 }),
    longDescription: options.longDescription ?? undefined,
    shortDescription: options.shortDescription ?? undefined,
    thumbnailUrl: options.thumbnailUrl ?? 'https://somewebiste.com/image.jpg',
    isbn: options.isbn ?? generate({ length: 10, charset: 'numeric' }),
    pageCount: options.pageCount ?? random(100, 500),
    categories: options.categories ?? [
      generate({ length: 20, charset: 'alphabetic' }),
    ],
    authors: options.authors ?? [
      generate({ length: 20, charset: 'alphabetic' }),
    ],
    status: options.status ?? 'PUBLISH',
    published: options.published ?? {
      $date: new Date().toISOString(),
      currency: 'USD',
      price: 100,
    },
  } as RawBook;
}
