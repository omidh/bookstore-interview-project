
## Description

 Interview project for [Queros](https://www.qureos.com) company based on Nest.

## Requirements

You can skip these if want to use docker.

1. NodeJs
2. MongoDB

## Installation if you are not using docker

```bash
$ npm install
```

copy `.env-example` to `.env` and change its content if needed.

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Running the app using docker

```bash
$ cd path/to/project/root

$ docker-compose up -d
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Endpoint

Service would be running on port 3000 unless specified otherwise in `.env` file.

## Swagger

You can access Swagger on `htpp://localhost:3000/api` and experiment with the app. Don't forget to import data first by passing JSON url to `import` endpoint.

## Stay in touch

- Author - Omid Habibi

## License

Nest is [MIT licensed](LICENSE).
